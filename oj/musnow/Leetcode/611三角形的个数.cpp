class Solution {
public:
    int triangleNumber(vector<int>& nums) {
        // 三角形是a+b>c
        // 先对原数组进行排序，然后选择一个c，再在它之后选a和b就一定能满足c<a+b
        sort(nums.begin(), nums.end());
        // 枚举所有数字作为c
        int count = 0;
        size_t n = nums.size();
        // 选择i和j作为左侧的a和b
        for (int i = 0; i < n; ++i) {
            // 三角形的边不可能为0，直接跳过
            if (nums[i] == 0) {
                continue;
            }
            for (int j = i + 1; j < n; ++j) {
                // 在这里二分查找c，找到最大的满足a+b>c的，即可得到组合数量
                // 这里求的就是nums[i] + nums[j] > nums[k];
                //
                // 这里k赋值为j是为了避免二分查找失败，比如nums[i]为0的时候，是没有符合条件的三角形的
                // 换句话说，我们二分查找的范围是[j+1,n-1]，此时k初始化为j相当于设置了一个没有意义的值
                // 最终如果二分查找失败（因为j右侧的都比nums[j]大），k没有被更新，结果数量就是k-j=0个；
                // 还有另外一种处理的方式是让nums[i]直接跳过0，三角形的边不能为0；
                int left = j + 1, right = n - 1, k = j;
                while (left <= right) {
                    int mid = (left + right) / 2;
                    if (nums[mid] < nums[i] + nums[j]) {
                        k = mid;
                        left = mid + 1;
                    } else {
                        right = mid - 1;
                    }
                }
                // 此时j和k之间的举例就是能选择作为c的数字个数。
                count += k - j;
            }
        }
        return count;
    }

    int triangleNumber2(vector<int>& nums) {
        // 三角形是a+b>c
        // 先对原数组进行排序，然后选择一个c，再在它之后选a和b就一定能满足c<a+b
        sort(nums.begin(), nums.end());
        // 枚举所有数字作为c
        int count = 0;
        size_t n = nums.size();
        // 选择i和j作为左侧的a和b
        for (int i = 0; i < n; ++i) {
            // 三角形的边不可能为0，直接跳过
            if (nums[i] == 0) {
                continue;
            }
            int k = i;
            for (int j = i + 1; j < n; ++j) {
                while (k + 1 < n && nums[k + 1] < nums[i] + nums[j]) {
                    k++;
                }
                if (k - j > 0) {
                    count += k - j;
                }
            }
        }
        return count;
    }
};