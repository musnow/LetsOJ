class Solution {
public:
    vector<int> dailyTemperatures(vector<int>& temperatures) {
        stack<int> st; // 单调栈
        // 这里初试化为全0，默认每个元素右边都么有比自己大的，这样结尾的元素不需要单独处理
        vector<int> retV(temperatures.size(), 0);
        // 思路如下，遍历的时候，将下标入栈
        // 1.当前元素大于栈顶元素（用下标访问得到元素大小），出栈顶元素
        //   并使用当前元素下标-栈顶元素下标得到距离，写入结果集
        // 2.当前元素小于等于栈顶元素，将下标入栈
        //
        // 这样能保障栈顶到栈底是递增的，也就能找到右边第一个比当前元素大的元素
        // 第一个元素直接入栈
        st.push(0);
        // 从第二个元素开始遍历
        for (int i = 1; i < temperatures.size(); i++) {
            // 大于
            if (temperatures[i] > temperatures[st.top()]) {
                // 只要栈内有元素，就需要继续弹出
                while (!st.empty() &&
                       temperatures[i] > temperatures[st.top()]) {
                    // cout << i << " " << st.top() << " | sz " << st.size() <<
                    // endl;
                    int topIndex = st.top();
                    st.pop();
                    // cout << i << " after pop | sz " << st.size() << endl;
                    retV[topIndex] = i - topIndex; // 赋值距离
                }
                // 栈为空，或者当前元素小于栈顶元素，可以入栈
                // cout << i << " | empty " << st.empty() << endl;
                // if(!st.empty()){
                //     cout << i << " | top " << st.top()<<"\n" ;
                // }
                if (st.empty() || temperatures[i] <= temperatures[st.top()]) {
                    // cout << "push " << i << endl;
                    st.push(i);
                }
            } else // 小于等于
            {
                st.push(i);
            }
        }
        // 到这里遍历结束了，但是栈里面还有元素
        // 此时栈里面的元素右边都不会有比自己更大的元素了，因为从栈顶到栈底是递增的
        // 而我们在初始化vector的时候已经初始化为全0了，也就不需要对这种情况单独处理
        return retV;
    }
};
