class Solution {
public:
    bool isMatch(string s, string p) {
        vector<vector<bool>> dp(s.size() + 1,
                                vector<bool>(p.size() + 1, false));
        // 初始化
        dp[0][0] = true;
        // 初试化
        for (size_t j = 1; j < dp[0].size(); j++) {
            // 如果是*则可以匹配，只要有不是*的就没办法匹配
            dp[0][j] = p[j - 1] == '*' && dp[0][j - 1];
            cout << j << " " << dp[0][j] << endl;
        }

        // 本题和leetcode10有些区别。主要在于*的处理。
        // 1.字符串是?可以匹配一个
        // 2.字符串是字母，直接比较
        // 3.字符串是*，不需要和前一个配对，直接进行比较
        //      1）匹配0个，则相当于当前字符不存在，沿用dp[i][j-1]
        //      2）匹配1个，相当于?，直接进行匹配，沿用dp[i-1][j]
        //      3）匹配2个以上，不存在这种情况。
        for (size_t i = 1; i < dp.size(); i++) {
            for (size_t j = 1; j < dp[i].size(); j++) {
                // 情况1，p[j-1] == '*'
                if (p[j - 1] == '*') {
                    // 1. '*'只匹配0个字符（相当于删除*）
                    bool result1 = dp[i][j - 1];
                    // 2.'*'匹配1个字符。
                    bool result2 = dp[i - 1][j];
                    // 3. '*'匹配2个和以上的字符
                    //    这种情况不用考虑！因为每次dp循环s只新增了一个字符，那里来的两个字符以上的匹配？

                    // 最终dp只要有一个情况为true就行
                    dp[i][j] = result1 || result2;
                }
                // 情况2，p[j-1] == '?' 或者是个字母
                else {
                    // 1. 是一个字母，直接判断二者是否相等
                    bool result1 = dp[i - 1][j - 1] && s[i - 1] == p[j - 1];
                    // 2. 是一个'.'，那和二者相等也是没区别的
                    bool result2 = dp[i - 1][j - 1] && p[j - 1] == '?';
                    dp[i][j] = result1 || result2;
                }
            }
        }

        return dp[s.size()][p.size()];
    }
};
